# 留言岛

#### 介绍
web端匿名留言墙，主要练练ui设计

#### 预览
![图床没了](https://i.hd-r.cn/459ace5ad329c23323524a94915149a5.png)

![图床没了](https://i.hd-r.cn/1a676c1c6673c55c41eab91012ed5d4c.png)
#### 软件架构
前端hcj，后端php，MySQL

#### 安装教程

MySQL需要自己创建个表，至于php中的 *lxy* 是开发时的数据库名，可自行修改为自己的

建表sql语句
```sql
CREATE TABLE IF NOT EXISTS `liuyan`(
  `id` INT UNSIGNED AUTO_INCREMENT COMMENT 'ID',
`name` varchar(10) CHARACTER SET utf8 NOT NULL,
 `logo` varchar(10) CHARACTER SET utf8 NOT NULL,
 `txt` varchar(200) CHARACTER SET utf8 NOT NULL,
 `ip` text(50) NOT NULL,
 `time` text(50) NOT NULL,
  PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

```

#### 使用说明

qq：2529855535 欢迎加我深入探讨 /滑稽

