let cleanHtml = Str => Str.replace(/<\/?.+?>/g,"");
  
let Tip = function (txt,align){
	//为了用户界面美观，仅限20字,超出字数不弹出提示，直接返回false
    // @txt 提示文本  @align value:0/1 正常/居中
    if(document.getElementsByClassName('_tip')[0] != undefined){return 0;}
	cleanHtml(txt);
	if(txt.length > 20){
		return 0;
	}else{
	    let opat = 1;
		let tip = document.createElement("div");
		let style = document.createAttribute("style");
		
     	tip.setAttribute("class","_tip");
		tip.style.zIndex = "9999";
		tip.style.padding = "8px 4px 8px 8px";
		tip.style.borderRadius = "4px";
		tip.style.opacity = opat;
		tip.style.width = "120px";
		tip.style.height = "auto";
		tip.style.color = "white";
		tip.style.textAlign = "left";
		tip.style.lineHeight = "auto";
		tip.style.backgroundColor = "#2c2c2c";
		tip.innerHTML = txt;
		tip.style.position = "fixed";
		tip.style.left = (window.innerWidth/2-60) + "px";
		tip.style.bottom = "50px";
		document.getElementsByTagName("body").item(0).appendChild(tip);
        if(align==1)  {tip.style.textAlign = "center";}
        let tout = setTimeout(()=>{ 
            let val = setInterval(()=>{
                opat -= 0.1;
                tip.style.opacity = opat;
                if(opat <= 0){
                    clearInterval(val);
			        clearTimeout(tout);
                    let body =document.getElementsByTagName("body").item(0);
			        let child = document.getElementsByClassName("_tip")[0];
			        body.removeChild(child);
			        
                }
            },100);
        },1000);
		
	    return true;
	}
}






